# goxic-tex

[![Test Coverage](https://img.shields.io/badge/coverage-0%25-red.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/goxic-tex)](https://goreportcard.com/report/codeberg.org/fractalqb/goxic-tex)
[![GoDoc](https://godoc.org/codeberg.org/fractalqb/goxic-tex?status.svg)](https://godoc.org/codeberg.org/fractalqb/goxic-tex)

`import "git.fractalqb.de/fractalqb/goxic-tex"`

---

goxic temlate parster and tools for LaTeX templates
